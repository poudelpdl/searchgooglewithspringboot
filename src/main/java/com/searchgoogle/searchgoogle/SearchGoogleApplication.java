package com.searchgoogle.searchgoogle;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;

@SpringBootApplication
public class SearchGoogleApplication extends WebMvcAutoConfiguration {

    public static void main(String[] args) {
        SpringApplication.run(SearchGoogleApplication.class, args);
    }

}
