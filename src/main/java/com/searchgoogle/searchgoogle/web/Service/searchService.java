package com.searchgoogle.searchgoogle.web.Service;



import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class searchService {
    /*
     * This method would send a request to search for a keyword and then parse the html response using a
     * parser called JSOUP and check if there is any particular URl in result and then obtain the count
     * */

    public int searchAndReturnParsedResponse() throws IOException  {
        int totalCount = 0;


        Document doc = Jsoup.connect("https://www.google.com/search?q=online%20title%20search&num=100").get();

        Elements results = doc.select("a[href]");

        for (Element result : results) {
            String linkHref = result.attr("href");
            if(linkHref.indexOf("infotrack") > -1){
                totalCount += 1;
            }
        }
        return totalCount;
    }
}
