package com.searchgoogle.searchgoogle.web;



import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController

public class searchController{

    com.searchgoogle.searchgoogle.web.Service.searchService searchService;

    @Autowired
    public void setUserService(com.searchgoogle.searchgoogle.web.Service.searchService searchService) {
        this.searchService = searchService;
    }

    @PostMapping("/search/searchProduct")
    public String searchProduct() {
        int totalCount = 0;
        try {
            totalCount = searchService.searchAndReturnParsedResponse();
        }catch(Exception e){
           System.out.println("Exception occurred:::::"+e);
        }
        return  JSONObject.quote(Integer.toString(totalCount));


    }

}
